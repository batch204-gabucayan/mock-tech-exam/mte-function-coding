function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    // count how many letters in a sentence
    // e.g a, banana = 3
    if(letter.length > 1) {
        return undefined
    } else {

        const chars = sentence.split("");

        for(let i = 0; i < sentence.length; i++ ){
            if(letter === chars[i]){
                result++;
            }else {
                continue;
            }
        }

        return result;
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no REPEATING LETTERS.
    // The function should DISREGARD TEXT CASING before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let letters = text.toLowerCase();

    for(let i = 0; letters.length > i; i++) {
        for(let j = i+1; letters.length > j; j++) {
            if(letters[i] === letters[j]) {
                return false
            }
        } 
    }
    return true

}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens(age > 64). (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // Resuls should always show two decimal places
    // The returned value should be a STRING.
    
    // if(age < 13) {
        // return undefined
    // }

    if(age < 13) {
        return undefined
    }else if(age <= 21 && age >= 13) {
        let discountedPrice = price*0.8
        return `${discountedPrice.toFixed(2)}`;
    }else if(age > 64) {
        let discountedPrice = price*0.8
        return `${discountedPrice.toFixed(2)}`;
    }else if(age >= 22 && age <= 64) {
        return `${price.toFixed(2)}`;
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let noStockCat = [];

    items.map((i) => {
        if(i.stocks === 0){
          if(!noStockCat.includes(i.category)) {
              noStockCat.push(i.category);
          }  
      }
        });
    return noStockCat;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let votedBoth = [];

    for(let i=0; i < candidateA.length; i++){
        if(candidateB.includes(candidateA[i])){
            votedBoth.push(candidateA[i]);
        }
    }
    return votedBoth;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};